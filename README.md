# Pottermore Sorting Hat Quiz Breakdown

[snidgetseeker.gitlab.io/pottermore](http://snidgetseeker.gitlab.io/pottermore)

Have you ever wondered why you were sorted into a particular Hogwarts house by the [Pottermore Sorting Hat](https://my.pottermore.com/sorting)? Would you have gotten into a different house if you'd been asked different questions?

This version of the quiz is based on the [recent breakdown](https://www.reddit.com/r/Pottermore/comments/8rgo4d/new_finding_on_the_grading_scheme_of_the_sorting) by [Niffler Felicis](https://www.reddit.com/user/N1ffler), who already made [replicas of all the Pottermore quizzes](http://wizardmore.com), but I wanted a version that'd show me how my answers were contributing to the result. Mine's not nearly as pretty to look at, but it does what I wanted it to do. (I've since realised that Niffler's [extended version](http://wizardmore.com/sorting-hat-x) is almost exactly the same as this one, but I think the quiz-counting mechanism is unique to mine.)

If you want to repeat a quiz that you took on Pottermore, then you can choose to answer one question from each section. This will show you what score you got for each house.

If you answer all the questions, then you'll also see how often you would've been placed in each house based on which of the questions were randomly selected during the quiz.

If you want to take the quiz without being influenced by knowing the score for each answer (or if you find the colours too distracting), then you can uncheck the `Show Key` checkbox.

If you accidentally answer a question you didn't mean to, you can select the `Don't answer` option for any question to pretend you never answered it.
